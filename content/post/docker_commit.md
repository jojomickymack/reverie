---
title: "Docker_commit"
date: 2019-02-21T09:25:26-07:00
draft: true
---

# Using Docker On The Commandline

When using docker to create and push images, you have two choices to accomplish the same thing.

- you can write a Dockerfile with various instructions, then run 'docker build .' and 'docker push'
- you can pull and run an existing image in a container, save it as an image using 'docker commit' and push the image to the registry

Doing it the second way is practical for several reasons, but it's tricky to figure out how to do it right, since it requires more commands.

## The General Idea

Docker runs 'images', which are like disks, in 'containers', which is like a disk drive. You can interact with the container while it's running to install packages and set up a project - for example you might want to have all a project dependencies installed, compile all your source code, and then save the container that way so when you pull it later (from a different machine maybe), it will be ready to run.

The steps involved are

- pulling a base image which is a starting point
- running the image in a container interactively - that means that you have a terminal open
- installing python and nano and writing a demo program
- exiting the terminal
- 'committing' the containers state to a new 'image'
- 'tagging' and 'pushing' the image to the registry

## What is a registry?

A registry is a place on the internet where docker images can be stored. Popular ones are hub.docker, gitlab, or quay.io.

## What does it mean to 'tag' an image?

If you do not give a tag to an image, it will default to 'latest'. A tag is really just a short word or version number to designate something unique about the image, and using 'latest' is generally a bad practice. The tag follows a colon, or ':' at the end of a registry url. An example is below - the tag in this url is :edge. Alpine is a popular linux image that only takes up about 3mb

```bash
alpine:edge
```

the one below is from gitlab

```bash
registry.gitlab.com/jojomickymack/cache_test01:my_tag
```

Tagging an image is done with a 'docker tag' command, or is done in combination with 'docker build' with the -t flag.

## Quick Demo

It can be confusing to get started with docker commands in general. I'd recommend doing this with a fresh install so you don't have to sort through a bunch of images and containers you already have on your system.

Keep in mind that you can see all images and containers on your system at any given time with the commands below.

```bash
docker image ls -a
docker container ls -a
```

The -a flag means 'all' - it's good to do this to avoid confusion since without that flag it only shows running containers, and most of the time your containers aren't running but we still want to see that they're there.

Let's pull the alpine image and run it in a container with a name that's obvious. The alpine image is extremely small, so it's trival to pull and remove it over and over for testing/experimentation.


```bash
docker image ls -a
docker pull alpine
docker image ls -a
docker run --name cool_container alpine
docker container ls -a
```

explanation
--name
what will be shown in names column when listing containers - if you don't supply this, it will generate a ridiculous one (and cause confusion)

-it
interactive shell

--rm
Automatically remove the container when it exits (do not do this unless you are just testing/experimenting)

ash
the shell command (on ubuntu this is bash, ash is the one alpine uses)

note that the status is exited, we want to do things with it this time.
remove the container since the command below creates it again

```bash
docker rm cool_container
```

start the container in interactive mode so its state can be changed

```bash
docker run -it --name cool_container alpine ash
```

alpine uses apk as its package manager - like apt-get install but usage is 'apk add [package]'/ 'apk rm [package]'

```bash
apk add python3
apk add nano
mkdir /home/app
cd /home/app
```

write a program

```bash
nano runme.py
exit
```

now back outside of thee container, make sure that it still exists.

```bash
docker container ls -a
```

the command below will create a new image from the current state of the container.

```bash
docker commit cool_container cool_image
docker image ls
```

note that your created image has a larger size than the alpine one you started with.
the command below tags and pushes the image you just created to a registry - this particular gitlab account is just for example, replace it with your own (gitlab accounts are free, as are hub.docker accounts).


```bash
docker tag cool_image registry.gitlab.com/jojomickymack/cache_test01:my_tag
docker push registry.gitlab.com/jojomickymack/cache_test01:my_tag
```

note that the layers take up the larger amount of space (it has the things you added)
also note that these commands require that you are authenticated to the registry, use commands below where the registry domain is wherever you are trying to push to


```bash
docker login registry.gitlab.com -u [username] -p [password]
```

or

```bash
docker login registry.gitlab.com
```

(it should ask you for credentials)

if this is successful, in your registry you should see your image with 'my_tag' as the tag

to avoid confusion, delete all containers and images

```bash
docker rm cool_container
docker rmi cool_image
docker rmi alping
docker rmi registry.gitlab.com/jojomickymack/cache_test01:my_tag
```

explanation
rm      remove a container
rmi     remove an image

now all the containers and images associated with this demo are gone, these commands shouldn't list anything now

```bash
docker container ls -a
docker image ls -a
```

now pull your image again and verify that what you installed is there


```bash
docker pull registry.gitlab.com/jojomickymack/cache_test01:my_tag
docker run -it --rm registry.gitlab.com/jojomickymack/cache_test01:my_tag ash
```

When using the image you should find that it's in the same state it was before you exited it previously - python3 and nano are installed, and your program should be in /home/app

congratulations - you've pulled a base image (alpine), installed some stuff, committed your changes to a new image, and then tagged and pushed the image to a docker image registry.

## Summary

You could've accomplished the same thing by creating a Dockerfile like this. This assumes that your python program is in the same directory as the Dockerfile.

```yml
FROM alpine
RUN add nano
RUN add python3
ENV APP_HOME=/home/app
ADD . $APP_HOME
WORKDIR $APP_HOME
```

The commands you'd run to tag and push it using the Dockerfile would be these (assuming you're in the same directory as the Dockerfile).

```bash
docker build -t registry.gitlab.com/jojomickymack/cache_test01:my_tag .
docker push registry.gitlab.com/jojomickymack/cache_test01:my_tag
```

It's two ways to accomplish the same thing.