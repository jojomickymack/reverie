---
title: "Ktx_cutscene"
date: 2019-03-19T08:18:19-06:00
draft: false
---

# LibKtx Cutscenes and Asset Management

One thing I spent some time trying to figure out with Libgdx is how to effectively stop the game and go into some sort of story-telling mode.

Think about this for a second - old arcade games traditionally had no narrative whatsoever. Consider the [original mario bros arcade cabinet](https://www.youtube.com/watch?v=ly8DofqCuOs) - like many others from the time the experience is purely driven by the game mechanics.

Compare that to [ninja gaiden](https://www.youtube.com/watch?v=_rkaiKYEkDQ), which sets the stage before the game starts with a magnificent cutscene. In that minute long intro, so much is conveyed that contributes to the player's experience. It has cinematograhy referencing Akira Kurosawa, and the text and music sets a dramitic mood and establishes who the protagonist is and why they're trying to prevail in the actual gameplay. 

One thing I've observed is that this type of storytelling puts the player in a different mode - one that has them open to make connections to films, literature, and their own experiences in life. It extends the abstraction of what are essentially symbols on the screen (the literal bitmap that represents the player, or a pickup, or a cloud in the sky) to something real or from their imagination. 

You can instantaneously put the player in that 'narrative mode' by showing them a cutscene, which doesn't need to be more elaborate than still images with text in order to be effective. Flourishes like transitions, characters entering the scene, short animations, text appearing and sound queues all speak volumes when the player is involved in the narrative.

## Overview

Technically, it's not hard at all to do those sorts of things with libgdx - in fact they've engineered an api called 'scene2d' which can be used for these purposes. 'Scene' is an object in libgdx - one that can contain many 'actors' that get updated and drawn when the Scene's 'act' and 'render' methods are called. Libgdx contains many pre-made classes that extend 'Actor', such as buttons, windows, and labels - but you can make your own classes like a character on screen extend 'Actor' as well.

'Actions' are ways you can assign some kind of change to an 'Actor' that progresses over time, and a 'Sequence' is a collection of 'Actions' that will play out synchronously, that is, the second action won't start until the first one has completed.

Using 'Actors', and 'Sequences' of 'Actions', you can orchestrate a section of a cutscene - and by swapping these short sections you'll have something very similar to the intro scene from ninja gaiden.

## What About Libktx?

What Libktx adds to these Libgdx is a shorthand for adding actions to sequences or actors, and adding those actors to a stage, which overloads the '+' or '+=' operators. What you'll end up with is something like the following.

```kotlin
var label = Label("Welcome To The Game", AppObj.skin, "default")
label += sequence(delay(2f) + fadeIn(2f)) + delay(2f) + fadeOut(2f)

stage += label
```

Another thing that libktx provides are some tricks for managing assets. 

## Why Do You Need to Think About Asset Management?

One of the things that's tricky about game development in general is dealing with all of the graphics, sounds, fonts, and other things that you display as your game is running. Those assets take up a lot more space than what a typical app would, which probably only consists of some layouts and logic. 

If your game has a lot of assets, you can have a lot of control over when different things get loaded into memory - for example, why would you load the music and images that are shown during the final boss fight when your game starts? Think about if the game goes into the background on an android device - do you want your app to have all of those assets still in memory if the user isn't even playing the game?

If you're just getting started and want to just see your own creation on the screen, you probably don't want to engineer your game to utilize an asset management system since it adds a lot of complexity - but it's something that you should at least know about.

The fact is, Libgdx gets a lot of it's performance because it's using native loading and unloading of assets - that means it uses compiled c++ to handle it, and the Java garbage collector is not going to free up the memory. For [all these types of objects](https://github.com/libgdx/libgdx/wiki/Memory-management) in libgdx, you need to make sure to call the dispose() method on them when your game no longer needs them.

That gives you a lot of responsibility to keep track of what assets your game uses and when they're loaded and unloaded. Fortunately, there's a class called 'AssetManager' which makes it so that's a little easier - all the assets you load into the AssetManager get freed when you call the AssetManager's dispose() method.

Again - if you're making a demo, experiment or a 'hello world', an AssetManager is likely to only get in the way - just make sure to remember that it exists once your game grows to a larger size and you start to have concerns about performance and not taking up more memory on a user's mobile device then your game needs to run.

## Without An Asset Manager

If you take a look at the [version without asset management](https://github.com/jojomickymack/alien-cutscene/blob/master/core/src/com/central/AppObj.kt), you can see an example of an approach you might take without using an AssetManager - this is a technique I use for games and other kinds of applications - make the entrypoint to your application called App.kt, and store all of your constants in an object right next to it called 'AppObj.kt'. You can see all the resources that get used throughout the lifecycle of the app all in a row and I can easily keep track of things that need to be disposed of when the game quits. They are easy to access at any time and in any file using a static reference to AppObj.

Let me decompose that a little - when you provide a way to access something statically, it's available anywhere - or globally. For games, this makes things a lot easier since so many parts of a game affect the state of other parts - for example, your Tiled Map needs to change the state of the player when they collide with a wall. The player needs to change the state of an enemy when he attacks him.

With a static resource, I can access it with AppObj.myResource from anywhere in the game - without using this I'd need to pass a reference around, which can get really messy.

This is totally fine for a small game, but there will be problems when is AppObj gets too big - what if I had 100 sounds instead of 3? Since I'm holding them statically in memory, all 100 sounds are going to be loaded from when the app is loaded until the dispose() method gets called.

## All The Assets Consolidated

If all of my assets are in the same place, all I have to do is instantiate an AssetManager inside of AppObj and load all of my sounds and images into it - then statically reference the asset through the AssetManager - see the [libgdx documentation](https://github.com/libgdx/libgdx/wiki/Managing-your-assets). Notice how you need to keep track of the types - obviously you can't store sounds and images in the same AssetManager without making sure to tell the AssetManager what the type is when you store or get the asset.

Libktx offers a pretty nice solution for this which you can see under 'implementation tip: type-safe assets' in [the documentation](https://github.com/libktx/ktx/tree/master/assets).

```kotlin
enum class Images {
  logo,
  player,
  enemy;

  val path = "images/${name}.png"
  fun load() = manager.load<Texture>(path)
  operator fun invoke() = manager.getAsset<Texture>(path)
  
  companion object {
    lateinit var manager: AssetManager
  }
}
```

This approach makes it so you can have one asset manager per type - I decided to do this for Images, Sounds, Skins, and 'Tunes' (that is Music - but be careful not to use the same name as a class that already exists in libgdx!). See [this directory](https://github.com/jojomickymack/alien-cutscene/tree/asset_management/core/src/com/central/assets).

To have this work, you need to sort your assets into directories of respective type and reference the individual files by their filename when you get them from the asset manager. This is actually one of the best parts of this approach - when you import this enum you can freely invoke the name of the file as if it were a function, as shown below.

```kotlin
val background = Image(ship())
```

Inside of the images directory among my assets - there is a 'ship.png' file that it just called getAsset\<Texture\>() on - pretty cool!

The real bonus comes in the dispose() method in my [App.kt](https://github.com/jojomickymack/alien-cutscene/blob/asset_management/core/src/com/central/App.kt) file - instead of having to remember to call dispose() on each individual asset, I can just call the AssetManager's dispose method and all of the assets are disposed. This is much nicer.

## Loading All Of The Files

Without asset management, just loading the files each scene used at the start of the scene was fine - they are tiny files anyway and they are ready by the time I need them. Again, with a larger game this won't always be the case - if you load something and immediately call methods on it you could end up crashing the game if the loading hasn't finished.

To use my asset enums, I import them to my App.kt file and set them all up

```kotlin
// create my AssetManagers as members

val skinsManager = AssetManager()
val textureManager = AssetManager()
val soundsManager = AssetManager()
val tunesManager = AssetManager()

// set the managers for each of my asset types (in the create method)

Skins.manager = this.skinsManager
Images.manager = this.textureManager
Sounds.manager = this.soundsManager
Tunes.manager = this.tunesManager

// also in the create method - call load() on all of the assets

Skins.values().forEach { it.load() }
Images.values().forEach { it.load() }
Sounds.values().forEach { it.load() }
Tunes.values().forEach { it.load() }

// in the dispose method

this.skinsManager.dispose()
this.textureManager.dispose()
this.soundsManager.dispose()
this.tunesManager.dispose()
```

The AssetManager allows for you to load and unload things on the fly so only what's needed is in memory - here I just loaded everything, but for a large game you'd only load what's needed in the screen that's about to be shown.

I created a [loading screen](https://github.com/jojomickymack/alien-cutscene/blob/asset_management/core/src/com/central/screens/Loading.kt) where I artificially make it take longer (otherwise you'd barely see it).

Like the official documentation says, the AssetManager will return false from its 'update' method if it's not done loading. I hold the whole game back until all of the assets are done loading - you do have to call update() in the if(!manager.update()) {} block or the loading will never progress. You can find out how much loading is left with the getProgress() method to show a bar filling up or something like that.

## Adding Some More

Now to add more assets, all you need to do is put it in the right folder, and add it to the Asset enum matching its type - then you can just call it by the filename() in your game!

## Watch Out For Static AssetManagers

One thing that's warned against in the libgdx documentation is that you don't want for your AssetManager to be a static asset - the reason is because it can grow to a huge size, and your game will crash if something happens to it - for example the Android runtime decides to garbage collect it because it's been sitting there in the background for awhile and the system needs to reclaim the memory for some reason.

In my example, I basically put everything that as in AppObj.kt into App.kt, and passed the instance of 'app' into each of my scenes - that way they can access the global state of the game by calling methods off of it.

## What Else

Other techniques that are used along the same lines are 'Texture Packing', where all of your images are combined into one and a json file defines which regions in the single texture represent which picture - this reduces loading time since there's only one thing to load - but it introduces some complexity because in order to add an additional texture you need to 'pack it' into the single texture again. In my opinion,  that would be a smart thing to add when you're done developing, but getting the pipeline right so the texture is built over and over is hard.

Another thing that can be done is [dependency injection](https://github.com/libktx/ktx/tree/master/inject). This is kind of a more overarching technique to eliminate your dependency on static assets - basically it makes it easy to keep objects out of scope until they need to be in scope. 

## Examples

Cutscene example which does not use an AssetManager

[https://github.com/jojomickymack/alien-cutscene](https://github.com/jojomickymack/alien-cutscene)

The same cutscene which does use an AssetManager and the ktx extensions

[https://github.com/jojomickymack/alien-cutscene/tree/asset_management](https://github.com/jojomickymack/alien-cutscene/tree/asset_management)