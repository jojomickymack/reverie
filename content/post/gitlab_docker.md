---
title: "Gitlab_docker"
date: 2019-02-17T07:47:26-07:00
draft: true
---

# Gitlab And Docker Integration

My company has gitlab-ce (Community Edition) installed on an internal domain, and I've installed gitlab-ce myself at home and on a dev server at work. It's not difficult to do. Right out of the box (by following the instructions [here](https://about.gitlab.com/install/#centos-7) you can log in and immediately start using it to host projects. With a little know-how, you can enable the 'pages' feature (which is actually what's hosting this blog), and begin taking advantage of the continuous integration features by including a .gitlab-ci.yml file in your project and installing gitlab-runners on remote machines.

I explained a lot of this before in a separate article, which has a bunch of simple .gitlab-ci.yml scripts to demonstrate many of the basic features. I'll be doing the same thing with this article showing some of the docker related features.

And then there's docker - a command line tool that's easy to install, but to use to it's potential is something of a paradigm shift. You can run anything in a docker container, and an application's different components really should be in separate containers. Docker can really change the angle at which you approach a problem, and brings a lot of compelling ideas to light that are part of dev-ops culture.

I'm reminded of the aphorism "a rising tide lifts all boats" when considering gitlab and docker. The concept of a pipeline encapsulates what being 'agile' as opposed to static is all about. On one hand you have the goal of achieving a stable and unchanging system that never needs to change, and on the other you have a fragmented system with components that can easily be changed seemlessly.

Figuring out how to get docker, gitlab-runners, and the gitlab docker registry to integrate required some learning on my part, so I'm recording what I learned here for future reference.

## The Basic Idea

Gitlab-ce comes with a docker image registry - this is a place where you can save your docker images so they can be pulled later and shared with others. There's a lot of places you could do this, like [docker hub](https://hub.docker.com) or [quay](https://quay.io). What these sites typically look like is website you can search for images on and store your own - gitlab is more like github repository with an option to store docker images that contain the repository inside of an image that already has all the environmental dependencies the project needs.

You might think to yourself that you don't really care about having a repo in gitlab, all you might be interested in is having a place to push your docker image to. You can absolutely do that - just create an empty project, click on the 'registry' tab, and follow the instructions to use docker to push an image to the registry.

In order to use docker in our builds, we need to have a gitlab-runner installed on a machine that has docker installed, we're going to have a Dockerfile the project that the runner is registered to, and we're going to have an optional build step that runs 'docker build' and 'docker push' commands so it updates a docker registry with an image that is ready to run because it has all dependencies, and a built project inside of it.

If that already makes sense to you, see the complete .gitlab-ci.yml file at the end of this article. If not, read on so I can explain more about each part of this.

## The Gitlab Runner

You can put whatever instructions you want in your .gitlab-ci.yml file in the context of the gitlab-runner that is registered to the gitlab project that contains it.

## Shell Executor

What do I mean by the 'context' of the gitlab-runner? The gitlab runner can be installed on any machine you want, and there are a number of different 'executors' you can select when registering a runner to listen for commands from a project. The two that I'm using are 'shell' or 'docker' executors. If you are running 'shell' script commands, you have access to all the variables and binaries in the $PATH that you would if you just typed them in on whatever machine the gitlab-runner is installed on.

## Docker Executor

For a 'docker' executor, you have access to whatever is available if you were to do an interactive session with whatever image you specified in that job's 'image' field. I've found myself stumped plenty of times when running commands in a job meant to be run either in my Dockerfile or in a .gitlab-ci.yml file and finding that they don't work. The way to reason about how things work in the docker context is by running the image in interactive mode with the -it flag and then just manually running the command you're confused about.

```bash
docker run -it [imagename] /bin/bash
```

Make sure that you understand how the -it flag works - it requires that the shell application be given as an argument - most of the time it's going to be /bin/bash.

Note: if the image you are running is based on 'alpine', a popular minimal linux image, it uses a spartan alternative called /bin/ash

It's also a good idea to make sure that there is a 'tag' required by the job each executor is intended to run of 'shell' or 'docker', since there isn't much else in the .gitlab-ci.yml file to clue you into the context of the executor.

A job that is meant to be run inside of a docker container will have an 'image' field, and though you could put any valid path to a docker registry there, it makes a lot of sense for that registry address to be the one associated with the gitlab registry. You will see the address in the push instructions shown on when you click on the 'registry' tab in gitlab.

It's a good idea to use any field you'll use in your .gitlab-ci.yml file more than once in a variable. To make the ci script re-usable, I put them at the top and reuse them for different projects since all that's likely to be different is the project name. That's why at the top of my .gitlab-ci.yml file I have this.

```yml
variables:
    USER: 'registry.gitlab.com/jojomickymack'
    REPO: 'dkr02'
    IMAGE: $USER/$REPO
```

The idea here is that the gitlab-runner will run scripts in the context of your docker image by using your project's image registry when it's in the 'image' field of a job that is to be run by a docker executor.

```yml
run:
    tags: ['docker']
    stage: run
    image: $IMAGE:$TAG
    script:
        - echo 'I am inside the container now'
```

## Keeping Your Config Handy

In order to avoid having manually run the 'gitlab-runner register' commands over and over again, I like to switch back and forth between running gitlab-runner in 'user-mode' for setup and debugging and 'system-mode' for set-and-forget usages.

If you download a gitlab-runner and just stick it in a directory and run it directly, you are using 'user-mode'. If you register it to a project, it will create a 'config.toml' file in that directory and run it when you do 'gitlab-runner run' directly. What I like to do is take the config.toml and include it with the project. You can just copy it into wherever the gitlab-runner runs from when it's running as a service (or on linux it's /etc/gitlab-runner), and it will assume that configuration immediately when started.

If you have gitlab-runner installed as a service already, you'll want to stop the service before doing that.

Also note that if you're using linux and have gitlab-runner installed as a service, whenever running gitlab-runner without sudo, you are using 'user-mode' and the config.toml file will appear in your user account's .gitlab-runner directory.

## Making Sure You're Authenticated To The Registry

registry.gitlab.com is what's used for www.gitlab.com - if you're using gitlab-ce installed on your own server it tends to be gitlab.something.com:5050. Keep in mind that whatever the domain is, you'll need to make sure docker is authenticated to push to it. Usually that's a matter of running 'docker login [domain] -u [username] -p [password]'. What that typically results in is the docker config file (in #HOME/.docker/config.json) getting an entry for that domain and a hash for decrypting your credentials.

Note: in linux environments a gitlab-runner is likely to be have its own user account. You may end up needing to assume the gitlab-runner user by running 'sudo su - gitlab-runner' and doing a 'docker login' from there, or just copying your own .docker/config.json file into the .docker directory in the gitlab-runner home directory. The point is, the docker config file is unique for each user running the command to start the runner.

## Keeping Things From Your Build As Artifacts And Hosting Them Using 'Pages'

Artifacts are things that continue to exist after your job has ended. It's common to preserve a project's build directory (for java) or node_modules directory (for nodejs) between 'build' and 'run' stages by making them into artifacts. 'cache' is very similar, but from reading [the documentation](https://docs.gitlab.com/ee/ci/caching) it has a narrower use case and isn't supposed to be for build files.

What's important to understand is that jobs do not exist in the same context of any other job. In the context of a job, no other jobs every occurred and this job is it's own thread having nothing to do with anything outside of itself.

For me, the use case for artifacts is to make it so the test results from the testing job are uploaded to gitlab so people can access them and for debugging. There's a couple of 'gotchas' I want to point out.

Unless you override it, if a job fails, no artifacts will be preserved. In the context of viewing test results, this is absurd. Make sure to put 'allow_failure: true' and 'when: always' in a job like this. Keep in mind that the html test results are set up to be created in a directory called 'public'.

```yml
run:
    tags: ['docker']
    stage: run
    image: $IMAGE:$TAG
    script:
    	- echo 'running my tests now'
    artifacts:
        when: always
        paths:
            - public
    allow_failure: true
```

The 'paths' field indicates where the files are that need to be saved. Note that 'public' is a folder name that has significance to the 'pages' job.

```yml
pages:
    tags: ['shell']
    stage: pages
    dependencies:
        - run
    when: always
    script:
        - echo 'I upload things to gitlab'
    artifacts:
        paths:
            - public

```

Note that the 'pages' job has the 'run' job as a dependency - this makes it so the artifacts from that job will be downloaded with the project files, which is why the 'pages' 'artifacts' step can assume that they're there.

As mentioned before, artifacts as a concept is used for more than just pages - anything that should be shared between jobs and accessible online from the gitlab 'ci' -> 'pipeline' or 'jobs' view is supported by artifacts.

While preserving build dependencies using artifacts is a solution to a problem, for me at least, it goes against why I'm using a docker image - that docker image is supposed to have my project and all it's dependencies _ready to run_, not _ready to build_. If you're running your pipeline over and over, you're probably thinking the same thing I am - 'why should I wait for dependencies to download and builds to be done if nothing is actually changing'?

That's what I'll go into next - how to get your docker image to contain all your dependencies and be ready to run as soon as you pull it.

## It All Goes In The Dockerfile

There are 2 ways to build an image - you can do it manually by doing an interactive session where you manually execute commands to install packages using whatever package manager the base image has - then exit and do a 'docker commit' command, then do a 'docker push' to the address of the image registry.

Note: Debian based images obviously utilize apt-get for installing packages - alpine linux has something different called 'apk' which works totally differently. Make sure you know what your image is based on to avoid confusion.

The other way, which is a best practice, is to put all of the commands to install packages and to get your project ready into a Dockerfile, and then run the whole thing with a 'docker build' command.

The result is the same, but with the Dockerfile you can easily share how an image is set up with others and tweak it as you need. Also keep in mind that there is a large cost in terms of how big your resulting image is if you change your mind about what is supposed to be installed while going the manual route - packages that are no longer installed are still tracked, making it so if you decide to uninstall something you committed earlier in favor of other packages, all of them are part of the image in the end.

I need to have google chrome installed in my image so I can run tests using a selenium in headless mode. That means I need to run these steps in my build. Note that gradle is the base image, which is an official docker image ([found here](https://hub.docker.com/_/gradle)).

```yml
FROM gradle
RUN apt-get update && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
&& echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update && apt-get -y install google-chrome-stable
```

Note how many of the apt-get commands are run together with &&. This makes the build more efficient, as each RUN directive is seen by docker as an independent build step.

Consider this - if I were to push this image, I would have google chrome installed when I pulled the image. If I were to change my build file so instead of pulling 'FROM gradle', and instead put in 'FROM [image registry url]', I could then skip the step of installing chrome.

That sounds like a smart thing to do, but it's not a best practice - your dockerfile is supposed to have _everything_ that is required to set up the image from scratch.

What I prefer to do is accept that it takes a while to install chrome and just only do a build when my dependencies change.

You can prevent a job from getting run in your .gitlab-ci.yml file by putting a period in front of it. Note that the variables use in the 'script' steps have % signs because the gitlab-runner is installed on windows - if your gitlab-runner is installed on linux, you'd use $ for those.

```yml
.docker build:
    tags: ['shell']
    stage: docker build
    script:
        - docker build -t %IMAGE%:%TAG% .
        - docker push %IMAGE%:%TAG%
```

That job won't run unless I take the period away from the job name.

This makes sense for me since I don't change the project a lot - but if you do, you might just want to run an official image like gradle, install your dependencies manually, then do 'docker commmit' and 'docker push' to your own registry in gitlab or wherever, then put 'FROM [registry url]' in your Dockerfile and do whatever else you want to do and take for granted that whatever dependencies are installed are there.

What I don't like about this is that if you push an image that doesn't have those dependencies in the future, your dependencies will just be gone unless you fix it by manually pushing again. In any case, you don't want to be stuck behind installing the same dependencies over and over and over.

## Gradle Dependencies And Build Files

Since I want my docker image to be ready to run with all dependencies included, I searched for solutions on how other people coped with this.

I found [this solution](https://stackoverflow.com/questions/25873971/docker-cache-gradle-dependencies) on stack overflow which outlined it.

I added a gradle task to download the dependencies with a slight tweak from one of the answers provided.

```gradle
task resolveDependencies {
    doLast {
        project.rootProject.allprojects.each { subProject ->
            subProject.buildscript.configurations.each { configuration ->
                configuration.resolve()
            }
            subProject.configurations.each { configuration ->
                if(configuration.canBeResolved) configuration.resolve()
            }
        }
    }
}
```
Sometimes it's difficult to figure out where you are when you start an image - that's why it's a good idea to create a directory where your app will live and set that as the working directory with WORKDIR. It's common to see the ADD command used instead of COPY to move everything that's in the same directory as the Dockerfile into that new directory.

That's what's happening in this part of my Dockerfile below.

```yml
ENV APP_HOME=app
ADD . $APP_HOME
USER root
RUN chmod -R 777 $APP_HOME
WORKDIR $APP_HOME
RUN gradle resolveDependencies -g $APP_HOME
RUN gradle build -x test
```

Gradle dependencies (jar files needed by the project) are typically downloaded into a hidden directory in the executor's home directory called .gradle/caches. Using the -g flag in my gradle command, I'm telling it to use the APP_HOME directory for that purpose.

Also note that I'd like to have all my class files built for me when I run the image. Since I'd prefer that my tests not get run, I pass '-x test' so they're skipped.

The result is that each time this is run, all my dependencies and build files are in the APP_HOME directory when the image gets run. This way, I never have to wait around for those things to be done over and over and over.

## The Whole Recipe

Below are all the files I used to get my gitlab build up and running with the docker registry. Keep in mind for this particular instance, the gitlab-runner was running on windows, hense the use of %VAR_NAME% instead of $VAR_NAME in the .gitlab-ci.yml shell script fields.

Keep in mind that there is a java project using gradle in this project directory, hense all the gradle commands.

If you would like to look at the project more closely or clone it, it's located at the url below.

[https://gitlab.com/jojomickymack/dkr0](https://gitlab.com/jojomickymack/dkr0)

## .gitlab-ci.yml

```yml
variables:
    USER: 'registry.gitlab.com/jojomickymack'
    REPO: 'dkr02'
    IMAGE: $USER/$REPO
    TAG: '0.0.1'
    BUILD_REF: $CI_BUILD_REF
    APP_DIR: '/home/gradle/app'
    CLONE_DIR: '/builds/jojomickymack/dkr02'

stages:
    - docker build
    - run
    - pages

.docker build:
    tags: ['shell']
    stage: docker build
    script:
        - docker build -t %IMAGE%:%TAG% .
        - docker push %IMAGE%:%TAG%

run:
    tags: ['docker']
    stage: run
    image: $IMAGE:$TAG
    script:
        - pwd
        - cd $APP_DIR
        - pwd
        - ls -la
        - chmod +x chromedriver
        - gradle test
        - mv public $CLONE_DIR
    artifacts:
        when: always
        paths:
            - public
    allow_failure: true

pages:
    tags: ['shell']
    stage: pages
    dependencies:
        - run
    when: always
    script:
        - echo 'I upload things to gitlab'
    artifacts:
        paths:
            - public
```

## Dockerfile

```yml
FROM gradle
RUN pwd
RUN ls -la
ENV APP_HOME=app
ADD . $APP_HOME
USER root
RUN chmod -R 777 $APP_HOME
WORKDIR $APP_HOME
RUN gradle resolveDependencies -g $APP_HOME
RUN gradle build -x test
RUN apt-get update && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
&& echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update && apt-get -y install google-chrome-stable

```

